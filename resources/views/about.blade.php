@extends('layout')



@section('content')

    
          <!-- Breacrumb Area -->
          <div class="breadcrumb-area-about" data-black-overlay="7">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                        <div class="cr-breadcrumb text-center">
                            <h1>About Us</h1>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Breacrumb Area -->

        <!-- Page Content -->
        <main class="page-content">

            <!-- About Area -->
            <section id="about-area" class="about-area section-padding-lg bg-grey">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-6 col-lg-6 order-2 order-lg-1">
                            <div class="about-content" style="text-align:center">
                                <h2 >Our Mission</h2>
                                <p>Simply put, we want to make sure that our clients become the best in their respective sectors 
                                and we will be the ROI multiplier for your company.</p>                                
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 order-2 order-lg-1">
                            <div class="about-content" style="text-align:center">
                                <h2 >Our Vision</h2>
                                <p>We want to be the company that brings a smile and a nod of approval from clients because
                                 of the passion they witness in how we harmonize talent and flawless delivery.</p>                                
                            </div>
                        </div>
                       
                    </div>
                </div>
            </section>
            <!--// About Area -->

       

            <!-- Skills Area -->
            <section id="skills-area" class="skills-area section-padding-lg bg-white">
                <h2 style="text-align:center;">Technology We Use</h2>
                <div class="container">
                    <div class="row align-items-center">                        
                         
                            <div class="col-lg-6">
                                <div class="skills-area-content p-0">                                 
                                    <div class="cr-progress-bar">
                                        <h6>Photoshop</h6>
                                        <div class="progress">
                                            <div class="progress-bar wow slideInLeft" data-wow-delay="0.2s" data-wow-duration="2s" role="progressbar" style="width: 85%; visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;"
                                                aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                            <span>85%</span>
                                        </div>
                                    </div>

                                    <div class="cr-progress-bar">
                                        <h6>Illustrator</h6>
                                        <div class="progress">
                                            <div class="progress-bar wow slideInLeft" data-wow-delay="0.2s" data-wow-duration="2s" role="progressbar" style="width: 90%; visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;"
                                                aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                            <span>90%</span>
                                        </div>
                                    </div>

                                    <div class="cr-progress-bar">
                                        <h6>After Effects</h6>
                                        <div class="progress">
                                            <div class="progress-bar wow slideInLeft" data-wow-delay="0.2s" data-wow-duration="2s" role="progressbar" style="width: 85%; visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;"
                                                aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                            <span>85%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">                              
                                    <div class="cr-progress-bar">
                                        <h6>Laravel</h6>
                                        <div class="progress">
                                            <div class="progress-bar wow slideInLeft" data-wow-delay="0.2s" data-wow-duration="2s" role="progressbar" style="width: 90%; visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;"
                                                aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                            <span>90%</span>
                                        </div>
                                    </div>

                                    <div class="cr-progress-bar">
                                        <h6>WordPress</h6>
                                        <div class="progress">
                                            <div class="progress-bar wow slideInLeft" data-wow-delay="0.2s" data-wow-duration="2s" role="progressbar" style="width: 70%; visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;"
                                                aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                            <span>70%</span>
                                        </div>
                                    </div>

                                    <div class="cr-progress-bar">
                                        <h6>Bootstrap</h6>
                                        <div class="progress">
                                            <div class="progress-bar wow slideInLeft" data-wow-delay="0.2s" data-wow-duration="2s" role="progressbar" style="width: 95%; visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;"
                                                aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                                            <span>95%</span>
                                        </div>
                                    </div>
                            </div>
                           

                       
                    </div>
                </div>
            </section>
            <!--//Skills Area  -->

            
        </main>
        <!--// Page Content -->

@endsection